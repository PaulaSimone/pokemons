FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt install apache2 -y \
    software-properties-common

# PHP
RUN LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php && apt-get update && apt-get install -y php7.3
RUN apt-get install -y \
    php7.3-curl \
    php7.3-dev \
    php7.3-xml \
    php7.3-mysql \
    php7.3-mbstring \
    php7.3-zip \
    php7.3-bz2 \
    php7.3-soap \
    php7.3-json \
    php7.3-intl \
    php-memcached
RUN command -v php

RUN a2enmod rewrite

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
# autorise .htaccess files
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
#RUN sed -i 's/var\/www/var\/www\/dev/' /etc/apache2/apache2.conf
RUN sed -i 's/var\/www\/html/var\/www\/html/' /etc/apache2/sites-available/000-default.conf

CMD apachectl -D FOREGROUND

WORKDIR /var/www/html
EXPOSE 80