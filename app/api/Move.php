<?php

class Move {
    private $x;
    private $y;
    private $path;

    public function __construct() {
        $this->x = 0;
        $this->y = 0;
        $this->path = array('0,0');
    }

    public function walk($moves) {
        $arrMoves = str_split($moves, 1);
        for($i=0; $i < count($arrMoves); $i++) {
            switch(strtolower($arrMoves[$i])) {
                case 'n':
                    $this->moveUp();
                break;
                case 's':
                    $this->moveDown();
                break;
                case 'o':
                    $this->moveLeft();
                break;
                case 'e':
                    $this->moveRight();
                break;
            }
            //$this->path[$this->x][$this->y] = true;
            array_push($this->path, $this->x .',' . $this->y);
        }
        //print_r($this->path);
        $return = new stdClass();
        $return->count = count(array_unique($this->path));
        $return->path = $this->path;
        header('Content-Type: application/json');
        echo json_encode($return, JSON_FORCE_OBJECT);
    }

    public function moveUp() {
        $this->y--;
    }
    public function moveDown() {
        $this->y++;
    }
    public function moveRight() {
        $this->x++;
    }
    public function moveLeft() {
        $this->x--;
    }
}

//$move = new Move($_POST['coordenates']);
$move = new Move();

//$move->walk('NNNNSSSSSSEEOOOOOOOOOOOOOOSSSSSSSSSSSOOOOOOONNNNNNNNNNNEEEEEEEEEEEEEE');
$move->walk($_POST['coordenates']);