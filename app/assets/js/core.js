var app = (function() {
    var self = {};

    self.init = function() {
        self.addKeyEvents();
        $('form').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: { coordenates: $('[name=coords]').val() },
                success: function(data) {
                    self.render(data);
                }
            });
        });
    };

    self.render = function(data) {
        $('.ctn-game').html('');
        var c = 0, r = 0, split, path;
        path = data.path;
        $('.counter').text(data.count);
        for(xy in path) {
            split = path[xy].split(',');
            console.log(xy, split);
            c = +split[0] + 16;
            r = +split[1] + 11;
            if($('[data-id=path' + c + '' + r + ']').length <= 0) {
                $('.ctn-game').append($('<span data-id="path'+ c + '' + r + '" class="path" style="grid-column:' + (c) + '; grid-row:' +  (r) + ';"></span>'))
            }
        }
        $('.ctn-game span:last-child').addClass('ash');
    }

    self.addKeyEvents = function() {
        
        var pos = '';
        $(document).keydown(function(e){
            var c, r;
            c = $('.ash').css('grid-column').split(' / ')[0];
            r = $('.ash').css('grid-row').split(' / ')[0];
            switch(e.which) {
                case 37: // left
                    //alert("left pressed");
                    pos = 'O';
                    c--;
                break;

                case 38: // up
                    //alert("up pressed");
                    pos = 'N';
                    r--;
                break;

                case 39: // right
                    //alert("right pressed");
                    pos = 'E';
                    c++;
                break;

                case 40: // down
                    //alert("down pressed");
                    pos = 'S';
                    r++;
                break;
                default: return; // exit this handler for other keys
            }

            if($('[data-id=path' + c + '' + r + ']').length > 0) {
                $('[data-id=path' + c + '' + r + ']').remove();
            }


            console.log($('#' + $('.ash').attr('data-id')).length);
            if($('[data-id=' + $('.ash').attr('data-id') + ']').length <= 1) {
                var clone = $('.ash').clone();
                clone.removeClass('ash');
                $('.ctn-game').append(clone);
                $('.counter').text($('.ctn-game span').length);
            }
            $('.ash').css('grid-column', c);
            $('.ash').css('grid-row', r);
            $('.ash').attr('data-id', 'path' + c + '' + r)
            
            var hist = $('[name=coords]').val();
            $('[name=coords]').val(hist + pos);
            
            
            //$('form').submit();
        });
    }


    return self;
}());

app.init();
