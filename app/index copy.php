<?php
// N = top
// S = bottom
// E = left
// O = right

if(isset($_POST["txtSequence"]) && $_POST["txtSequence"]) {
	$strSequece = $_POST["txtSequence"];
	$arrInput = str_split($strSequece, 1);
	
	$arrPositionsGot = array("0,0");
	$curPosX = 0;
	$curPosY = 0;
	
	/*echo "<pre>";
	print_r($arrInput);
	echo "</pre>";*/
	
	for($i=0; $i < count($arrInput); $i++) {
		if($arrInput[$i] == "N" ) {
			$curPosY = $curPosY - 1;
		}
		elseif($arrInput[$i] == "S" ) {
			$curPosY = $curPosY + 1;
		}
		elseif($arrInput[$i] == "O" ) {
			$curPosX = $curPosX - 1;
		}
		elseif($arrInput[$i] == "E" ) {
			$curPosX = $curPosX + 1;
		}
		
		array_push($arrPositionsGot, $curPosY .",". $curPosX);
	}
	
	$arr = array_unique($arrPositionsGot);
	
	ob_start();
	echo "All positions passed through";
	echo "<pre>";
	print_r($arrPositionsGot);
	echo "</pre>";
	
	echo "Removed duplicated";
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
	$output = ob_get_clean();
}

?>
<html>

<head>
    <link rel="stylesheet" href="css/index.css">
    <script src="js/index.js"></script>
    <style>
    body {
        display: grid;
        grid-template-columns: 1fr 2fr;
        height: 100%;
    }

    .map {
        display: grid;
        grid-template-columns: repeat(51, 1fr);
        grid-template-rows: repeat(51, 1fr);
    }

    .mark {
        
    }

	.path {
		background: orange;
	}
	.start {
		background: green;
	}
	.last {
		background: red;
	}
    </style>
</head>

<body>
    <div class="wrp-form">
		Foram encotrados <?php echo count($arr); ?> pokemons
        <form action="#" method="post">
			<input type="text" name="txtSequence" value="<?=$strSequece ?>" />
            <br />
            <input type="submit" name="btnSubmit" />
            <?php
		echo $output;
	?>
        </form>
    </div>
    <div class="map">
		<?php
		$current = 0;
		$last = count($arr) - 1;
		$counter = 0;
		foreach($arr as $path) {
			//echo $path;
			list($y, $x) = explode(',', $path);
			$class = 'path';
			switch($counter) {
				case 0:
					$class = "start";
				break;
				case $last:
					$class = "last";
				break;
				default:
					$class = "path";
			}
			echo '<div class="mark ' . $class . '" style="grid-column:'. ($x + 25) .'; grid-row:' . ($y + 25) . ';"></div>';
			$counter++;
		}
	?>
    </div>
</body>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script>
	var pos = '';
	$(document).keydown(function(e){

    switch(e.which) {
		case 37: // left
			//alert("left pressed");
			pos = 'O';
        break;

		case 38: // up
			//alert("up pressed");
			pos = 'N';
        break;

		case 39: // right
			//alert("right pressed");
			pos = 'E';
        break;

		case 40: // down
			//alert("down pressed");
			pos = 'S';
		break;
		
		

        default: return; // exit this handler for other keys
	}
	var hist = $('[name=txtSequence]').val();

	$('[name=txtSequence]').val(hist + pos);
	$('form').submit();
	
});
</script>

</html>