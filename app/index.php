<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="assets/css/common.css">
    <script src="js/index.js"></script>
</head>

<body>
	<main>
		<div class="ctn-game">
			<span data-id="path1611" class="path ash" style="grid-column:16; grid-row:11;"></span>
		</div>
		<div class="ctn-terminal">
			<form action="api/Move.php" method="post">
				<label><span>Coordenadas:</span>
					<input type="text" name="coords" />
				</label>
				<button type="submit">calcular</button>
				<span class="counter"></span>
			</form>
		</div>
	</main>
	<script
		src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
		crossorigin="anonymous"></script>
	<script type="text/javascript" src="assets/js/core.js"></script>
</body>

</html>