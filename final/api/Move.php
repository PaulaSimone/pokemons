<?php

class Move {
    private $x;
    private $y;
    private $path;

    public function __construct() {
        $this->x = 0;
        $this->y = 0;
        $this->path = array('0,0');
    }



    public function proccess($moves) {
        $arrMoves = str_split($moves, 1);
        for($i=0; $i < count($arrMoves); $i++) {
            switch(strtolower($arrMoves[$i])) {
                case 'n':
                    $this->moveUp();
                break;
                case 's':
                    $this->moveDown();
                break;
                case 'o':
                    $this->moveLeft();
                break;
                case 'e':
                    $this->moveRight();
                break;
            }
            array_push($this->path, $this->x .',' . $this->y);
        }
        $return = new stdClass();
        $return->count = count(array_unique($this->path));
        $return->path = $this->path;

        return $return;
        
    }
    public function walk($moves) {
        $coords = $this->proccess($moves);
        header('Content-Type: application/json');
        echo json_encode($coords, JSON_FORCE_OBJECT);
    }
    public function walkCount($moves) {
        $coords = $this->proccess($moves);
        echo $coords->count;
        echo "\r\n";
    }

    public function moveUp() {
        $this->y--;
    }
    public function moveDown() {
        $this->y++;
    }
    public function moveRight() {
        $this->x++;
    }
    public function moveLeft() {
        $this->x--;
    }
}

$move = new Move();

$coordenates = '';

if(isset($_POST['coordenates'])) {
    $coordenates = $_POST['coordenates'];
    $move->walk($coordenates);
} else if(isset($_GET['coordenates'])) {
    $coordenates = $_GET['coordenates'];
    $move->walkCount($coordenates);
} else if(isset($argv[1])) {
    parse_str($argv[1], $_GET);
    $coordenates = $_GET['coordenates'];
    $move->walkCount($coordenates);
}
