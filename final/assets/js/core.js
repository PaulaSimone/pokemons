var app = (function() {
    var self = {};

    self.init = function() {
        self.addKeyEvents();
        $('form').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: { coordenates: $('[name=coords]').val() },
                success: function(data) {
                    self.render(data);
                }
            });
        });
    };
	
	self.render = function(data) {
        $('.counter').text(data.count);
    }

    self.addKeyEvents = function() {
        
		$('[name=coords]').keypress(function(e) {
			keypressed = e.which;
			if (keypressed == 115 || keypressed == 110 || keypressed == 101 || keypressed == 111 || keypressed == 69 || keypressed == 78 || keypressed == 79 || keypressed == 83 || keypressed == 8 || keypressed == 13)
				return true;
			else 
				return false;
		});
		
		$('[name=coords]').keyup(function(e) {
			
			$('[name=coords]').val($('[name=coords]').val().toUpperCase());

		});
    }


    return self;
}());

app.init();
