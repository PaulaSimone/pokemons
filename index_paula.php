<?php
// N = top
// S = bottom
// E = right
// O = left

$strSequece = "";
$arrTotal = array();
$total = 0;

if(isset($_POST["txtSequence"]) && $_POST["txtSequence"]) {
	$strSequece = $_POST["txtSequence"];
	$arrInput = str_split($strSequece, 1);
	
	$arrPositionsGot = array("0,0");
	$curPosX = 0;
	$curPosY = 0;
	
	for($i=0; $i < count($arrInput); $i++) {
		if($arrInput[$i] == "N" ) {
			$curPosY = $curPosY - 1;
		}
		elseif($arrInput[$i] == "S" ) {
			$curPosY = $curPosY + 1;
		}
		elseif($arrInput[$i] == "E" ) {
			$curPosX = $curPosX + 1;
		}
		elseif($arrInput[$i] == "O" ) {
			$curPosX = $curPosX - 1;
		}
		
		array_push($arrPositionsGot, $curPosY .",". $curPosX);
	}
	
	$arrTotal = array_unique($arrPositionsGot);
	
	echo "All positions passed through";
	echo "<pre>";
	print_r($arrPositionsGot);
	echo "</pre>";
	
	echo "Removed duplicated";
	echo "<pre>";
	print_r($arrTotal);
	echo "</pre>";
	
	$total = count($arrTotal);
} else {
	
}
?>
<html>
<head>
<link rel="stylesheet" href="css/index.css">
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript">
</script>
</head>
<body>
<form action="#" method="post">
<input type="text" id="txtSequence" name="txtSequence" onkeypress="return CharAllowed(event);" onkeyup="return upperCase(this);" value="<?=$strSequece ?>" />
<br />
<input type="submit" name="btnSubmit" />
</form>
<br />
Número de Pokemons no total: <?php echo count($arrTotal); ?>
</body>
</html>